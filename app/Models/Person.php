<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $departamento
 * @property string $localidad
 * @property string $municipio
 * @property string $nombre
 * @property int $anios_activo
 * @property string $tipo_persona
 * @property string $tipo_cargo
 */
class Person extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['departamento', 'localidad', 'municipio', 'nombre', 'anios_activo', 'tipo_persona', 'tipo_cargo'];

}
