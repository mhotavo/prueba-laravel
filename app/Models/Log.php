<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre_buscado
 * @property float $porcentaje_buscado
 * @property string $estado_ejecucion
 */
class Log extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['nombre_buscado', 'porcentaje_buscado', 'estado_ejecucion', 'codigo_http', 'registros_encontrados'];

}
