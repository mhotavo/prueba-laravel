<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use App\Models\log;

class SearchController extends Controller
{


    public function Log(Request $request)
    {
        $validator = $request->validate([
            'uuid' => 'required|integer',
        ]);
        if ($validator) {

            $log = Log::where('id', $request->uuid)->first();

            if (count(array($log)) > 0) {

                if ($log->registros_encontrados > 0) {
                    $people = Person::all();
                    $array = array();
                    if (count($people) > 0) {
                        foreach ($people as $key => $person) {
                            $person->nombre = trim(strtolower($person->nombre));
                            similar_text($log->nombre_buscado, $person->nombre, $porcentaje);
                            if ($porcentaje >= $log->porcentaje_buscado) {
                                array_push(
                                    $array,
                                    array(
                                        'percent' => $porcentaje,
                                        'persona' => $person
                                    )
                                );
                            }
                        }
                    }



                    $log->registros = $array;
                }


                return response([
                    $log
                ]);
            } else {
                return response([
                    "estado" => "No se encontraron registros",
                    "code" => "204",
                ]);
            }
        } else {
            return response([
                "estado" => "Parametros no validos",
                "code" => "400",
            ]);
        }
    }


    public function search(Request $request)
    {
        try {

            $validator = $request->validate([
                'nombre' => 'required',
                'porcentaje' => 'required|numeric|between:1,100',
            ]);

            if ($validator) {
                $request->nombre = trim(strtolower($request->nombre));
                $people = Person::all();
                $array = array();
                if (count($people) > 0) {
                    foreach ($people as $key => $person) {
                        $person->nombre = trim(strtolower($person->nombre));
                        similar_text($request->nombre, $person->nombre, $porcentaje);
                        if ($porcentaje >= $request->porcentaje) {
                            array_push(
                                $array,
                                array(
                                    'percent' => $porcentaje,
                                    'persona' => $person
                                )
                            );
                        }
                    }
                } else {
                    // No hay registros en al tabla
                    $response = ([
                        "estado" => "No se encontraron registros",
                        "code" => "204",
                    ]);
                }

                if (count($array) > 0) {
                    $response = ([
                        "estado" => "Registros encontrados",
                        "total_coincidencias" => count($array),
                        "coincidencias" => $array,
                        "code" => "200",
                    ]);
                } else {
                    $response = ([
                        "estado" => "Sin coincidencias",
                        "code" => "200",
                    ]);
                }
            } else {
                $response = ([
                    "estado" => "Parametros no validos",
                    "code" => "400",
                ]);
            }
        } catch (\Exception $e) {
            $response = ([
                "estado" => $e->getMessage(),
                "code" => "500"
            ]);
        }

        $log = new Log();
        $log->nombre_buscado = $request->nombre;
        $log->porcentaje_buscado = $request->porcentaje;
        $log->estado_ejecucion = $response['estado'];
        $log->codigo_http = $response['code'];
        if (!isset($response['total_coincidencias'])) {
            $response['total_coincidencias'] = 0;
        }
        $log->registros_encontrados = $response['total_coincidencias'];
        $log->save();
        $response['uuid'] = $log->id;

        return response($response, $response['code']);
    }
}
