php artisan key:generate
php artisan jwt:secret
php artisan migrate
php artisan db:seed --class=UserSeeder
php artisan db:seed --class=PeopleTableSeeder