<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DateTime; 

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password'  => bcrypt('admin'),               
                'remember_token' => NULL,
				'created_at' => new DateTime,
				'updated_at' => new DateTime,
            )
        ));
    }
}
