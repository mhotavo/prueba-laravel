<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('departamento', 100);
            $table->string('localidad', 100)->nullable();
            $table->string('municipio', 100)->nullable();
            $table->string('nombre', 200)->index('nombre');
            $table->integer('anios_activo')->nullable()->default(0);
            $table->string('tipo_persona', 100)->nullable();
            $table->string('tipo_cargo', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
