<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nombre_buscado', 200)->nullable();
            $table->double('porcentaje_buscado')->nullable();
            $table->string('estado_ejecucion', 100)->nullable();
            $table->integer('codigo_http')->nullable();
            $table->integer('registros_encontrados')->nullable()->default(0);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
